const navLinks = document.querySelectorAll('a.scroll-trigger');
const navMargin = 150;

/* scroll to target */
addEventListenerList(navLinks, 'click', () => {
  event.preventDefault();
  let targetElement = document.querySelector(event.target.hash);
  window.scroll({
    behavior: 'smooth', left: 0, top: targetElement.offsetTop - navMargin
  });
});


/* locate current scroll position */
const sections = document.querySelectorAll(".nav-scrollable");
const sectionMargin = 300;
var activeItem = null;

window.addEventListener("scroll", () => {
  let current = getCurrent();
  if (current != activeItem) {
    for (let i = 0; i < navLinks.length; i++) {
      i != current ? navLinks[i].classList.remove('active') : navLinks[current].classList.add('active');
    }
    activeItem = current;
  }
});

function getCurrent() {
  for (let i = sections.length - 1; i >= 0; i--) {
    if (window.scrollY >= sections[i].offsetTop - sectionMargin) {
      return i;
    }
  }
  return 0;
}


const themeButtons = document.querySelectorAll('.dropdown-menu>a');
for (let i = 0; i < themeButtons.length; i++) {
  themeButtons[i].addEventListener('click', () => {
    document.body.className = themeButtons[i].id + ' transition';
    localStorage.setItem('theme', themeButtons[i].id);
  });
}
const theme = localStorage.getItem('theme');
if (theme) {
  document.body.className = theme;
}

/* Change tab content on click */
const conocimHeader = document.querySelector('#conocimientos .tab-content .header');
const conocimContent = document.querySelector('#conocimientos .tab-content .body');
let indexActivo = 0;
let conocimTabs = document.querySelectorAll('#conocimientos .tab-list li');
conocimHeader.innerHTML = 'Front-End';
conocimContent.innerHTML = conocimData[0];

addEventListenerList(conocimTabs, 'click', () => {
  if (indexActivo != event.target.id) {
    conocimTabs[indexActivo].classList.toggle('active');
    indexActivo = event.target.id;
    conocimHeader.innerHTML = event.target.innerHTML;
    conocimContent.innerHTML = conocimData[indexActivo];
    event.target.classList.toggle('active');
  }
});


/* Project pop-up */
const projects = document.querySelectorAll('.project-list .project-item');
const overlay = document.querySelector('.overlay');


let popupShowing = false;
let cerrarPopup = () => {
  overlay.classList.toggle('mostrar');
  overlay.innerHTML='';
  popupShowing = false;
}

addEventListenerList(projects, 'click', () => {
  if (!popupShowing) {
    overlay.classList.toggle('mostrar');
    let currentProject = projectsData[event.target.id];
    let projectCard = document.createElement('div');
    projectCard.className = 'project-card';
    projectCard.addEventListener('click', ()=> {
      event.stopPropagation();
    });

    let titulo = currentProject.titulo;
    let tags = currentProject.tags;
    let preview = currentProject.preview;
    let text = currentProject.text;
    
    let enlaces = '';
    if (typeof currentProject.github != undefined) {
      enlaces = '<a target="_blank" class="gh-icon" href="' + currentProject.github + '"></a>';
    } if (typeof currentProject.live != undefined) {
      enlaces += '<a target="_blank" class="lv-icon" href="' + currentProject.live + '">Ver Live</a>';
    }
    
    projectCard.innerHTML= '<div class="header">'+ titulo +'</div>' + 
    '<div class="tags">' + tags + '</div>' + 
    '<div class="video-wrapper"><video src="recursos/video/'+ preview +'" autoplay muted loop></video>' + 
    '<div class="project-links">' + enlaces + '</div></div>' + 
    '<div class="body">' + text + '</div>';

    let close = document.createElement('div');
    close.className = 'close';
    
    close.innerHTML = 'Volver';
    close.addEventListener('click', cerrarPopup);
    overlay.addEventListener('click', cerrarPopup);

    projectCard.appendChild(close);
    overlay.appendChild(projectCard);
    
    popupShowing = true;
  }
});



function addEventListenerList(list, event, fn) {
  for (var i = 0; i < list.length; i++) {
    list[i].addEventListener(event, fn, false);
  }
}