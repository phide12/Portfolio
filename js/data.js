const projectsData = {
  cloudGallery: {
    titulo: "Galería de Imágenes Online",
    tags: "Web app, PHP(CodeIgniter), Javascript, HTML/CSS",
    text: "Web App para visualizar exposiciones online, permite a los usuarios crear y compartir <b>dibujos</b> mediante un <b>editor gráfico</b> propio, crear cuentas y hacer comentarios/valoraciónes sobre las exposiciones",
    preview: "cloudGallery.mp4",
    github: "https://github.com/Phide12/Proyecto-fin-grado",
    live: "https://memoriter-governmen.000webhostapp.com/"
  },
  travelTour: {
    titulo: "Web Reservas de Alojamientos",
    tags: "Web app, PHP(Zend-Framework), Javascript(Bootstrap, Jquery), HTML/CSS",
    text: "Web App que permite registrar usuarios, hacer reservas de alojamientos, crear una <b>factura en PDF de forma dinámica</b>, enviar incidencias y mas cosas relacionadas con la gestión/usuarios",
    preview: "traveltour.mp4",
    github: "https://github.com/Phide12/proyectoFCT",
    live: ""
  },
  naveJs: {
    titulo: "Juego Nave Javascript",
    tags: "Javascript(Canvas), HTML/CSS",
    text: "Juego para el navegador donde puedes mover una nave con las teclas, mostrara el estado del movimiento en un panel lateral",
    preview: "naveJs.mp4",
    github: "https://github.com/Phide12/navesJS",
    live: "https://phide12.github.io/navesJS/"
  },
  plantillaWeb: {
    titulo: "Plantilla Responsive",
    tags: "Javascript, HTML/CSS",
    text: "Plantilla web para una página de programacíon con seccion de Inicio, Cursos, Contacto y Conocenos. Utiliza <b>Media-Querys</b> en CSS para adaptar sus elementos al tamaño de pantalla",
    preview: "plantillaWeb.mp4",
    github: "https://github.com/Phide12/PlantillaWeb",
    live: "https://phide12.github.io/PlantillaWeb/"
  },
  calculadora: {
    titulo: "Calculadora Visual",
    tags: "Javascript(Canvas), HTML/CSS",
    text: "Calculadora con la que puedes <b>representar funciones</b> en un canvas, aparte de esto se puede cambiar la apariencia y usarla como calculadora científica",
    preview: "calculadora.mp4",
    github: "https://github.com/Phide12/Calculadora_JS",
    live: "https://phide12.github.io/Calculadora_JS/"
  },
  navesJava: {
    titulo: "Juego Arcade Java",
    tags: "Java, Juego",
    text: "Juego en Java simulando los arcades de marcianos y naves, puedes moverte hacia los lados y disparar y evitar que te golpeen los asteroides",
    preview: "navesJava.mp4",
    github: "https://github.com/Phide12/navesJava"
  },
};
const conocimData = [
  'Es mi campo favorito, me encanta tanto diseñar interfaces como hacer funcionalidades o juegos que se me ocurren en el navegador. <p class="highlight">Conozco : </p> HTML, CSS, JavaScript ES6+, Bootstrap, Jquery, Gsap(animaciones)',
  'Domino gran parte de este lenguaje, he trabajado en profundidad con php durante el grado creando el back-end de varias aplicaciones web y los proyectos finales. <p class="highlight"> Frameworks: </p> Codeigniter, Laravel, Zend Framework',
  'He trabajado con Java extensamente dentro del grado y tambien con JavaServer Pages (JSP) para utilizarlo como back-end en servidores Tomcat. <p class="highlight">Conozco: </p> Java y JSP',
  'Algunas de las tecnologías y herramientas con las que he trabajado para desarrollar. <p class="highlight">Conozco: </p>Visual Studio Code, Git, MySQL, Windows y Linux',
  'Tengo experiencia en el campo de las artes tanto en diseño como dibujando <a target="_blank" href="https://dribbble.com/jmtorr">Algunos de mis trabajos relacionados</a>. <br><br> Utilizo <b>Figma</b> para el diseño de paginas web y la creación de logos/imagenes SVG <p class="highlight"> Conozco: </p> Figma, Photoshop, Gimp<br>'
]
