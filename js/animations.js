const hero = document.querySelector(".hero-container");
const headline = document.querySelector(".hero-container .hero-headline ");
const slider = document.querySelector(".slider");
const tl = new TimelineMax();

tl.fromTo(hero, 1.6, { height: '0%' }, { height: '61%', ease: Power2.ease })
  .fromTo(hero, 1.2, { width: '90%' }, {width: '100%', ease: Power2.ease })
  .fromTo(hero, 1, { height: '61%' }, { height: '100%', ease: Power2.ease })
  .fromTo(slider, 2.5, { width: '0%' }, { width: '100%', ease: Power2.easeInOut }, '-=3.2')
  .fromTo(headline, 1.2, { opacity: 0, x: -80 }, { opacity: 1, x: 0 }, '-=2.2')